const express    = require('express');        // call express
const app        = express();                 // define our app using express
const bodyParser = require('body-parser');
const SHA256 = require("crypto-js/sha256");
var cors = require('cors');



var port = process.env.PORT || 8010;
var router = express.Router();

class Block {
    constructor(index, timestamp, data, previousHash = '') {
        this.index = index;
        this.previousHash = previousHash;
        this.timestamp = timestamp;
        this.data = data;
        this.hash = this.calculateHash();
    }

    calculateHash() {
        return SHA256(this.index + this.previousHash + this.timestamp + JSON.stringify(this.data)).toString();
    }
}


class Blockchain{
    constructor() {
        this.chain = [this.createGenesisBlock()];
    }

    createGenesisBlock() {
        return new Block(0, "01/01/2017", "Genesis block", "0");
    }

    getLatestBlock() {
        return this.chain[this.chain.length - 1];
    }

    addBlock(newBlock) {
        newBlock.previousHash = this.getLatestBlock().hash;
        newBlock.hash = newBlock.calculateHash();
        this.chain.push(newBlock);
    }

    isChainValid() {
        for (let i = 1; i < this.chain.length; i++){
            const currentBlock = this.chain[i];
            const previousBlock = this.chain[i - 1];

            if (currentBlock.hash !== currentBlock.calculateHash()) {
                return false;
            }

            if (currentBlock.previousHash !== previousBlock.hash) {
                return false;
            }
        }

        return true;
    }
}

// certificate.chain[1].hash = certificate.chain[1].calculateHash();


var certificate = new Blockchain();
certificate.addBlock(new Block(0, "20/07/2014", { id : 1 }));
certificate.addBlock(new Block(1, "02/07/2015", { id : 2 }));
certificate.addBlock(new Block(2, "10/07/2017", { id : 3 }));
certificate.addBlock(new Block(3, "12/07/2012", { id : 4 }));

app.use(cors());

app.use('/:hex', function(req, res) {
    var status ='';
    if (req.params.hex=='c3e885b592bffcb5702ac55aaeb0f5792058f7b0946b998e8caa0572a8846eae' && req.query.id==3 ||
        req.params.hex=='6681e4d17e2d3bfb5cdf813d02fc6ebba33de1bef38a976b5584af0d3f3ebd58' && req.query.id==2 ||
        req.params.hex=='c0f7126d990874adbef1bd781c0f89ea572ae8037d84af1fc1e2788a475cf39f' && req.query.id==1
    ){
        console.log("Hit!", req.params.hex, req.query.id);
        status = 'verified'
    }else{
        console.log("Unverified hit!", req.params.hex, req.query.id);
        status = 'Unverified'
    }
    res.json({ status: status});
});

app.use('/', function (req,res) {
    res.render("Hii..")
});

// START THE SERVER
// =============================================================================
app.listen(port);
console.log('Magic happens on port ' + port);
